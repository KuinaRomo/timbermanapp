﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bajarTiempo : MonoBehaviour {

    [SerializeField] GameObject Timber;
       
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!Timber.GetComponent<deathTimber>().death && Timber.GetComponent<deathTimber>().start) { 
            GetComponent<Image>().fillAmount = GetComponent<Image>().fillAmount - 0.004f;
        }
        else {
            GetComponent<Image>().fillAmount = 0.5f;
        }
        if(GetComponent<Image>().fillAmount <= 0) {
            Timber.GetComponent<deathTimber>().death = true;
        }
    }
}
