﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class createTree : MonoBehaviour {

    private int contadorHijos;
    private int random;
    private BoxCollider2D[] boxcolliders;
    public List <GameObject> trozosTronco;
    public List<GameObject> initialPosition;
    [SerializeField] private Sprite tronco;
    [SerializeField] private Sprite troncoIzq;
    [SerializeField] private Sprite troncoDer;
    [SerializeField] private Image time;
    private GameObject instantiated;
    bool cambiado;
    [SerializeField] GameObject Timber;
    private int trozoActual;
    private string positionTimber;
    private int points;
    public int trozosCortados;
    [SerializeField] Text score;
    private int numMalo;
    private int times;
    private bool recolocado;
    private float yPosition;
    [SerializeField] GameObject canvas;
    public float canvasw;
    public float canvash;
    public float lastY;
    private float TimberX;
    private float TimberY;
    private float TimberZ;


    //ArrayList myList = new ArrayList();

    // Use this for initialization
    void Start () {
        TimberX = Timber.GetComponent<RectTransform>().position.x;
        TimberY = Timber.GetComponent<RectTransform>().position.y;
        TimberZ = Timber.GetComponent<RectTransform>().position.z;
        recolocado = false;
        yPosition = -379.3f;
        numMalo = 4;
        times = 0;
        trozoActual = 0;
        positionTimber = "left";
        contadorHijos = 0;
        trozosTronco = new List<GameObject>();
        initialPosition = new List<GameObject>();
        trozosCortados = 0;
        foreach (Transform child in transform) {
            trozosTronco.Add(child.gameObject);
            initialPosition.Add(child.gameObject);
        }
        lastY = initialPosition[(initialPosition.Count - 1)].GetComponent<RectTransform>().position.y;
        canvasw = canvas.GetComponent<RectTransform>().rect.width;
        canvash = canvas.GetComponent<RectTransform>().rect.height;
    }

    // Update is called once per frame
    void Update() {
        if(Timber.GetComponent<deathTimber>().death && !recolocado) {
            Timber.GetComponent<RectTransform>().position = new Vector3(TimberX, TimberY, TimberZ);
            Timber.GetComponent<RectTransform>().localScale = new Vector3(-1, 1, 1);
            for (int i = 0; i < initialPosition.Count; i++) {
                initialPosition[i].GetComponent<RectTransform>().position = new Vector3(0 + (canvasw / 2), yPosition + (canvash / 2), 0);
                yPosition += initialPosition[i].GetComponent<RectTransform>().rect.height;
            }
            recolocado = true;
            yPosition = -379.3f;
            trozoActual = 0;
        }
        else if (!Timber.GetComponent<deathTimber>().death) {
            recolocado = false;
        }
        if (!Timber.GetComponent<deathTimber>().start) {
            points = 0;
            score.text = "" + points;
        }
    }

    public void moveLeft() {
        if (!Timber.GetComponent<deathTimber>().death && Timber.GetComponent<deathTimber>().start) {
            Timber.GetComponent<RectTransform>().position = new Vector3(TimberX, TimberY, TimberZ);
            Timber.GetComponent<RectTransform>().localScale = new Vector3(-1, 1, 1);
            trozosTronco[trozoActual].GetComponent<Animator>().SetBool("left", true);
            if (trozosTronco[trozoActual].GetComponent <postionTronco>().rama) {
                trozosTronco[trozoActual].GetComponent<BoxCollider2D>().enabled = false;
            }
            bajarArbol();
            if (trozoActual < (trozosTronco.Count - 1)) {
                trozoActual++;
            }
            else {
                trozoActual = 0;
            }
            trozosCortados++;
            time.fillAmount = time.fillAmount + 0.1f;
            subirPoints();
        }
    }

    public void moveRight() {
        if (!Timber.GetComponent<deathTimber>().death && Timber.GetComponent<deathTimber>().start) {
            Timber.GetComponent<RectTransform>().position = new Vector3((TimberX * -1) + ((canvash*67) /100), TimberY, TimberZ);
            Timber.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            trozosTronco[trozoActual].GetComponent<Animator>().SetBool("right", true);
            if (trozosTronco[trozoActual].GetComponent<postionTronco>().rama) {
                trozosTronco[trozoActual].GetComponent<BoxCollider2D>().enabled = false;
            }
            bajarArbol();
            if (trozoActual < (trozosTronco.Count - 1)) {
                trozoActual++;
            }
            else {
                trozoActual = 0;
            }
            trozosCortados++;
            time.fillAmount = time.fillAmount + 0.1f;
            subirPoints();
        }
    }

    void bajarArbol() {
        foreach (Transform child in transform) {
            if(!child.GetComponent<Animator>().GetBool("left") && !child.GetComponent<Animator>().GetBool("right")) {
                child.GetComponent<RectTransform>().position = new Vector3(0, 
                    child.GetComponent<RectTransform>().position.y - 
                    child.GetComponent<RectTransform>().rect.height, 0);
            }
        }
    }

    void subirPoints() {
        points++;
        score.text = "" + points;
    }
}
