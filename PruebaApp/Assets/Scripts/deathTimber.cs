﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deathTimber : MonoBehaviour {

    [SerializeField] GameObject GameOver;
    [SerializeField] GameObject buttonStart;
    public bool death;
    public bool start;
    private float contador;

	// Use this for initialization
	void Start () {
        death = false;
        start = false;
        contador = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (death) {
            contador += Time.deltaTime;
            GameOver.SetActive(true);
        }
        if(contador > 5) {
            start = false;
            death = false;
            buttonStart.SetActive(true);
            GameOver.SetActive(false);
            contador = 0;
        }
	}

    private void OnTriggerEnter2D(Collider2D collision) {
        death = true;
        GameOver.SetActive(true);
    }

    public void startGame() {
        start = true;
        death = false;
        GameOver.SetActive(false);
        buttonStart.SetActive(false);
    }
}
