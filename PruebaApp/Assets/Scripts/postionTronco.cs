﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class postionTronco : MonoBehaviour {

    public int position;
    [SerializeField] GameObject father;
    [SerializeField] GameObject Timber;
    public bool rama;
    int mult;
    private float height;
    private float canvash;
    private float lastY;
    private float positionPiece;

	// Use this for initialization
	void Start () {
        height = this.GetComponent<RectTransform>().rect.height;
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void recolocar() {
        canvash = father.GetComponent<createTree>().canvash;
        lastY = father.GetComponent<createTree>().lastY;
        positionPiece = lastY - (canvash / 2);
        this.GetComponent<Animator>().SetBool("left", false);
        this.GetComponent<Animator>().SetBool("right", false);
        if (!Timber.GetComponent<deathTimber>().death) {
            mult = father.GetComponent<createTree>().trozosCortados;
            father.GetComponent<createTree>().trozosCortados = father.GetComponent<createTree>().trozosCortados - 1;
            if (mult == 0) {
                mult++;
            }
           this.GetComponent<RectTransform>().position = new Vector3(0, (positionPiece - (height * (mult - 1))) + (canvash / 2), 0);
            if (rama) {
                this.GetComponent<BoxCollider2D>().enabled = true;
            }
        }
        else {
            mult = 0;
        }
        
    }
}
